#!/bin/bash

mkdir data
mkdir data/db

docker build -t cucumbot-mongodb .

docker run -d -p 27017:27017 --mount type=bind,source="$(pwd)"/../data/db,target=/data/db cucumbot-mongodb
