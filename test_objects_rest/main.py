"""

	Title
	-----

	Main script of TestObjects REST Service

	Description
	-----------

	Handles requests to the service
	- CRUD test steps
	- CRUD test cases
	- CRUD test suites

	Info
	----

	* author: adrianpothuaud

"""

from app import app, mongo
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import jsonify, request
from werkzeug import generate_password_hash, check_password_hash

#####################################
#		1)	STEPS					#
#####################################

# 1.1	Create
# -----------------------------------

"""
	- read request body
	- parse as step object
	- save step object to db
	- return status and info
"""
@app.route('/test-steps', methods=['POST'])
def add_test_step():
	_json = request.json
	_name = _json['name']
	_description = _json['description']
	if _name and _description:
		id = mongo.db.test_step.insert({
			'name': _name,
			'description': _description
		})
		resp = jsonify('Test step added successfully !')
		resp.status_code = 201
		return resp
	else:
		return not_found()

# 1.2	Read (all)
# -----------------------------------

"""
	- read test steps from db
	- parse to json
	- return status and list
"""
@app.route('/test-steps', methods=['GET'])
def test_steps():
	test_steps = mongo.db.test_step.find()
	resp = dumps(test_steps)
	return resp

# 1.3	Read (one)
# -----------------------------------

"""
	- read test step from db
	- parse to json
	- return status and details
"""
@app.route('/test-steps/<id>', methods=['GET'])
def test_step(id):
	test_step = mongo.db.test_step.find_one({'_id': ObjectId(id)})
	resp = dumps(test_step)
	return resp

# 1.4	Update
# -----------------------------------

"""
	- read test step from request body
	- update existing test step on db
	- return status and info
"""
@app.route('/test-steps/<id>', methods=['PUT'])
def update_test_step(id):
	_json = request.json
	_name = _json['name']
	_description = _json['description']
	# validate the received values
	if _name and _description:
		# save edits
		mongo.db.test_step.update_one({'_id': ObjectId(id['$oid']) if '$oid' in id else ObjectId(id)}, {'$set': {'name': _name, 'description': _description}})
		resp = jsonify('Test step updated successfully!')
		resp.status_code = 200
		return resp
	else:
		return not_found()

# 1.5	Delete
# -----------------------------------

"""
	- get id
	- delete in db
	- return status and info
"""
@app.route('/test-steps/<id>', methods=['DELETE'])
def delete_test_step(id):
	mongo.db.test_step.delete_one({'_id': ObjectId(id)})
	resp = jsonify('Test step deleted successfully!')
	resp.status_code = 204
	return resp

#####################################
#		2)	ERRORS					#
#####################################

# 2.1	404
# -----------------------------------

"""

"""
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

#####################################
#		3)	MAIN					#
#####################################

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
