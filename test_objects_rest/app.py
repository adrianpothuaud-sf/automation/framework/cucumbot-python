import os
from flask import Flask
from flask_pymongo import PyMongo

app = Flask(__name__)
app.secret_key = "secret key"
app.config["MONGO_URI"] = "mongodb://" + os.environ['MONGODB_HOSTNAME'] + ":27017/cucumbot"
mongo = PyMongo(app)
